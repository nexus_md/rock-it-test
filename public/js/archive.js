$(document).ready(function () {
    $('.btn-calc').click(function (e) {

        var route = $(this).attr('data-extract');
        var title = $(this).text();

        $('#modal .modal-title').html(title);
        $('#modal .modal-body').addClass('preload');


        $.ajax({
            url: route,
            method: 'GET',

            success:function (response) {
                $('#modal .modal-body').removeClass('preload');
                $('#modal .modal-body').html(response);
            },

            error:function () {
              alert('we have an error here, please report it to us');
              $('#modal .modal-body').removeClass('preload');
            }

        })

    });
});