<?php

namespace App\Http\Controllers;

use App\Calculations;
use App\Http\Requests\CalculationsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalculationsController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
	    $calculations = Auth::user()
	                        ->calculations()
		                    ->search($request)
	                        ->orderBy('id', 'DESC')
	                        ->paginate(50);

	    return view('user.calculations.index')->withData($calculations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CalculationsRequest $request) {

		Auth::user()->calculations()->create($request->all() );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $calculation = Auth::user()->calculations()->findOrFail($id);
//	    dd($calculation->content);
	    return view('user.calculations.show')->withData($calculation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
