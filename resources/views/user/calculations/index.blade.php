@extends('layouts.app')

@section('content')
    <div id="archive-page" class="container">

        <form action="{{ route('calculations.index', app()->getLocale()) }}" method="GET" class="form-inline mb-2">
            <div class="form-group col-12">
                <input type="text" class="form-control mb-3" id="search" placeholder="Search for a log" name="search">
                <button type="submit" class="btn btn-primary mb-3">Search</button>
            </div>

        </form>

        <div id="calcs" class="row">
            @foreach($data as $value)
                <div class="calc-wrap col-6 col-md-4">

                    <div class="card-header" id="heading">
                        <h5 class="mb-0">
                            <button type="button" data-extract="{{ route('calculations.show', $value->id) }}"
                                    class="btn btn-link btn-calc" data-toggle="modal" data-target="#modal">
                                {{ $value->title }}
                            </button>
                        </h5>
                    </div>

                </div>
            @endforeach

            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalLabel">

                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop


@section('js')
    @parent
    <script type="text/javascript" src="{{ asset('js/archive.js') }}"></script>
@stop
