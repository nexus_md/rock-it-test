$(document).ready(function () {

    var resultsToSave = [];
    //The main calc function
    $('#calculator').submit(function (e) {
        e.preventDefault();

        var calcs = $('.viewer').val();

        try{
            var result = math.eval(calcs);
            //Displaying the result + calc to user
            $('#results').prepend('<p>' + calcs + ' = ' + result + '</p>');
            $('.viewer').val(result);
            //Creating the object we will send to db
            resultsToSave.push(calcs + ' = ' + result);
        }

        catch (e) {
            alert('The operation is wrong, please review it and try again');
        }

    });

    //Clearing the calc log
    $('.clear-log').click(function () {
        $('#results').text('');
        resultsToSave = [];
    });

    //Saving the calc log
    $('#save-log').submit(function (e) {
       e.preventDefault();
       var route = $(this).attr('action');
       var logName = $('input[name="log-name"]').val();
       var token = $('input[name="_token"]').val();

       $.ajax({
           url: route,
           method: 'POST',
           data: {
               'title': logName,
               'content': resultsToSave,
               '_token': token
           },

           success:function(){
               alert('The log ' + logName + ' was created successfully');
           },

           error:function(){
               alert('Please fill the name field and make at least one calculation');
           }
       })
    });

});
