<div class="container">
    <form id="calculator" class="form-inline mb-3">
        <div class="form-group col-12">
            <input type="text" name="calculator" class="viewer col-8 form-control mr-3">
            <button id="equals" class="equals btn btn-primary" type="submit" data-result="">=</button>
        </div>
    </form>

    @if(Auth::check())
        <form id="save-log" action="{{ route('calculations.store') }}" class="form-inline mb-3 col-12">
            <div class="form-group pr-1">
                {!! csrf_field() !!}
                <input type="text" name="log-name" placeholder="Log Name" class="form-control">
            </div>
            <button class="save-log btn btn-primary mr-1" type="submit">Save Log</button>
            <button class="clear-log btn btn-secondary">Clear Log</button>
        </form>
    @endif
    <div class="col-12">
        <code id="results"></code>
    </div>
</div>


@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/calculator.css') }}"/>
@stop

@section('js')
    @parent
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/5.0.0/math.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/calculator.js') }}"></script>
@stop