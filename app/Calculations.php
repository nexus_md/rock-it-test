<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Calculations extends Model
{

	protected $fillable = [
		'title',
		'content'
	];

	protected $casts = [
		'content' => 'array'
	];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

	public function scopeSearch( $query, Request $request )
	{
		if ( $request->search ) {
			return $query->where( 'title', 'like', "%{$request->search}%" );
		}
	}
}
